import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FooModule } from '@component/foo';
import { JipModule } from '@component/jip';

import { UserStoreModule } from '@store/user';

import { AppComponent } from './app.component';
import { AppStoreModule } from './app-store.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppStoreModule,
    BrowserModule,
    FooModule,
    JipModule,
    UserStoreModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
