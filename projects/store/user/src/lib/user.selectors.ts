import { createFeatureSelector, createSelector } from '@ngrx/store';
import { key, State } from './user.reducer';

const selectUserState = createFeatureSelector<State>(key);

export const selectUsers = createSelector(
  selectUserState,
  state => state ? state.users : []
);
