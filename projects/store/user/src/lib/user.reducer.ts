import { Action, createReducer, on } from '@ngrx/store';
import { UserInterface } from '@interfaces/user';
import * as userActions from './user.actions';

export const key = 'user';

export interface State {
  users: UserInterface[];
  count: number;
}

export const initialState: State = {
  count: null,
  users: null
};


const userReducer = createReducer(
  initialState,
  on( userActions.getUserSuccess, (state, { items, count } ) => {
    return ({ ...state, users: [ ...items ], count });
  })
);

export function reducer(state: State | undefined, action: Action): State {
  return userReducer( state, action );
}
