import { NgModule } from '@angular/core';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { UserEffects } from './user.effects';

import * as userReducer from './user.reducer';

@NgModule({
  imports: [
    StoreModule.forFeature( userReducer.key, userReducer.reducer ),
    EffectsModule.forFeature([ UserEffects ])
  ]
})
export class UserStoreModule { }
