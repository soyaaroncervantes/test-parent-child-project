import { createAction, props } from '@ngrx/store';
import { FooInterface } from '@interfaces/foo';

export const getUser = createAction('[ Api/User ] Get User');
export const getUserSuccess = createAction('[ Api/User ] Get User Success', props<FooInterface>() );
export const getUserFailure = createAction('[ Api/User ] Get User Success', props<any>() );
