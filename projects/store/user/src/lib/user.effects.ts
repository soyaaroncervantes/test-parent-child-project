import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType } from '@ngrx/effects';

import { ApiFooService } from '@services/api-foo';
import { catchError, map, mergeMap } from 'rxjs/operators';

import * as userActions from './user.actions';
import { of } from 'rxjs';


@Injectable()
export class UserEffects {

  constructor(
    private actions$: Actions,
    private apiFooService: ApiFooService
  ) {}


  getUserEffect$ = createEffect( () => this.actions$
    .pipe(
      ofType( userActions.getUser ),
      mergeMap(
        () => this.apiFooService.data
          .pipe(
            map( ({ items, count }) => userActions.getUserSuccess({ items, count }) ),
            catchError( err => of( userActions.getUserFailure({ err }) ) )
          )
      )
    )
  );
}
