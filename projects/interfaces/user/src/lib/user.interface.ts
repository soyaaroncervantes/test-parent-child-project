export interface UserInterface {
  id: string;
  createdAt: Date;
  name: string;
  avatar: string;
}
