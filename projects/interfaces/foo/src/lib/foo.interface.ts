import { UserInterface } from '@interfaces/user';

export interface FooInterface {
  count: number;
  items: UserInterface[];
}
