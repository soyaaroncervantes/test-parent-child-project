import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ApiFooService } from './api-foo.service';


@NgModule({
  imports: [ HttpClientModule ],
  providers: [ ApiFooService ]
})
export class ApiFooModule { }
