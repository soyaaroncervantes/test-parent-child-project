import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FooInterface } from '@interfaces/foo';

@Injectable({
  providedIn: 'platform'
})
export class ApiFooService {

  private url = 'https://5f90ba3be0559c0016ad6cb1.mockapi.io/v1';

  constructor(
    private httpClient: HttpClient
  ) { }

  get data(): Observable<FooInterface> {
    return this.httpClient.get<FooInterface>( `${ this.url }/users` );
  }

}
