import { TestBed } from '@angular/core/testing';

import { ApiFooService } from './api-foo.service';

describe('ApiFooService', () => {
  let service: ApiFooService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiFooService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
