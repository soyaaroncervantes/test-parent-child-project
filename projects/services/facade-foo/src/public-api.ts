/*
 * Public API Surface of facade-foo
 */

export * from './lib/facade-foo.service';
export * from './lib/facade-foo.module';
