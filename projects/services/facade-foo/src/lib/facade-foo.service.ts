import { Injectable } from '@angular/core';
import { UserInterface } from '@interfaces/user';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as fromUser from '@store/user';

@Injectable({
  providedIn: 'root'
})
export class FacadeFooService {

  constructor(
    private store: Store
  ) { }

  data(): void {
    this.store.dispatch( fromUser.getUser() );
  }

  get users$(): Observable<UserInterface[]> {
    return this.store.select( fromUser.selectUsers );
  }
}
