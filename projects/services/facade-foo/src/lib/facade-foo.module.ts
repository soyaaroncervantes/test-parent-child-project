import { NgModule } from '@angular/core';
import { FacadeFooService } from './facade-foo.service';
import { ApiFooModule } from '@services/api-foo';

@NgModule({
  imports: [ ApiFooModule ],
  providers: [ FacadeFooService ]
})
export class FacadeFooModule { }
