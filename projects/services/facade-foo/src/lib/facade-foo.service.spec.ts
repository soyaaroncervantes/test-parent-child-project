import { TestBed } from '@angular/core/testing';

import { FacadeFooService } from './facade-foo.service';

describe('FacadeFooService', () => {
  let service: FacadeFooService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FacadeFooService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
