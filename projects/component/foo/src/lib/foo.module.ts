import { NgModule } from '@angular/core';
import { FooComponent } from './foo.component';
import { CommonModule } from '@angular/common';
import { FacadeFooModule } from '@services/facade-foo';

@NgModule({
  declarations: [FooComponent],
  imports: [
    CommonModule,
    FacadeFooModule
  ],
  exports: [FooComponent]
})
export class FooModule { }
