import { Component, OnInit } from '@angular/core';
import { FacadeFooService } from '@services/facade-foo';
import { Observable } from 'rxjs';
import { UserInterface } from '@interfaces/user';

@Component({
  selector: 'component-foo',
  templateUrl: './foo.component.html',
  styleUrls: ['./foo.component.scss']
})
export class FooComponent implements OnInit {

  users$: Observable<UserInterface[]>;

  constructor(
    private facadeFooService: FacadeFooService
  ) {
    this.facadeFooService.data();
    this.users$ = this.facadeFooService.users$;
  }

  ngOnInit(): void { }

}
