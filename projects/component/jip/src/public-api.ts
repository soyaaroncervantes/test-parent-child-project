/*
 * Public API Surface of jip
 */

export * from './lib/jip.service';
export * from './lib/jip.component';
export * from './lib/jip.module';
