import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JipComponent } from './jip.component';

describe('JipComponent', () => {
  let component: JipComponent;
  let fixture: ComponentFixture<JipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
