import { TestBed } from '@angular/core/testing';

import { JipService } from './jip.service';

describe('JipService', () => {
  let service: JipService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JipService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
